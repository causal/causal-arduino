//#define DEBUG
#include <causalino.h>
#include <neotimer.h>
#include <Servo.h>

struct ServoTurner {
private:
  Servo servo;
  
  int angle = 0;
  bool up = true;

public:
  ServoTurner(unsigned int port) {
    servo.attach(port);
    servo.write(this->angle);
  }

  bool next() {
    switch(angle) {
      case 0:
        if(this->up) this->angle = 90;
        else return false;
        break;
      case 90:
        if(this->up) this->angle = 180;
        else this->angle = 0;
        break;
      case 180:
        this->up = false;
        this->angle = 90;
        break;
    }
    return true;
  }

  bool turn() {
    servo.write(this->angle);
  }
};

Neotimer timer = Neotimer(1000);

Branch b;
int val= 0;
bool up = true;
bool done = false;

bool canTurn() {
  return timer.done();
}

void turn(Branch& b, ServoTurner* st) {
  if(st->next()) {
    st->turn();
    b.act(canTurn, turn, st);
  } else {
    delete st;
  }

  timer.start();
}

void measure(Branch& b) {
#ifdef DEBUG
  Serial.print("done:\t"); Serial.print(done); Serial.print('\n');
  Serial.print("time.done():\t"); Serial.print(timer.done()); Serial.print('\n');
#endif
  
  if(done && timer.done()) return;
    
#ifdef DEBUG
  Serial.print("reading analog in\n");
#endif
  int pot = analogRead(A0);
  float potR = pot * (5.0/1023.0);
  Serial.print(potR); Serial.print('\n');
  
#ifdef DEBUG
  Serial.print("acting measure\n");
#endif
  b.act(measure);
}

void setup() {
  Serial.begin(9600); // 9600 baud is more than enough

  b.act(measure);
  b.act(canTurn, turn, new ServoTurner(11));
  timer.start();
}

void loop() {
  if(b.alive())
    b.tick();
}
