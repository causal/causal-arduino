//#define DEBUG
#include <causalino.h>
#include <neotimer.h>
#include <Servo.h>

struct ServoDriver {
private:
  const unsigned int port;
  unsigned int ain = 0;
  Servo servo;
  
  int angle = 90;
  bool up = true, sensitive = false;
  float scoreUp = 0.0, scoreDown = 0.0, gradientUp = 0.0, gradientDown, max = 0.0, min = 0.0, last = 0.0;
  unsigned int count = 0, hit = 0;

public:
  ServoDriver(unsigned int a, unsigned int p) : ain(a), port(p) {
    servo.attach(this->port);
    servo.write(this->angle);
  }

  void next() {
    Serial.print(this->scoreUp); Serial.print(';'); Serial.print(this->scoreDown); Serial.print('|');
    Serial.print(this->gradientUp); Serial.print(';'); Serial.print(this->gradientDown); Serial.print('|');
    Serial.print(this->max); Serial.print(';'); Serial.print(this->min); Serial.print('\n');
    if(this->scoreUp > 2.0 && this->gradientUp > 0.1)
      this->hit++;
    else
      this->hit = 0;

    if(this->hit > 4) {
      if(this->sensitive) {
        Serial.print("SWITCH\n");
        this->angle = this->up ? 0 : 180;
        this->up = !this->up;
        this->sensitive = false;
      }
    } else {
      this->sensitive = true;

      switch(this->angle) {
        case 0:
          this->angle = 5;
          this->up = true;
          break;
        case 180:
          this->angle = 175;
          this->up = false;
          break;
        default:
          if(this->up)
            this->angle += 5;
          else
            this->angle -= 5;
          break;
      }
    }
    this->reset();
  }

  void turn() {
    servo.write(this->angle);
  }

  void reset() {
    this->scoreUp = this->scoreDown = 0.0;
    this->gradientUp = this->gradientDown = 0.0;
    this->max = this->min = 0.0;
    this->last = 0.0;
    this->count = 0;
  }

  float measure() {
    int pot = analogRead(this->ain);
    float potD = (pot * (5.0/1023.0))-3.3;
    if(potD > 0)
      this->scoreUp += potD;
    else
      this->scoreDown += potD;

    if(potD > this->max)
      this->max = potD;
    else if(potD < this->min)
      this->min = potD;

    if(this->count > 0) {
      auto gr = potD-this->last;
      if(gr > 0.0)
        this->gradientUp = (this->gradientUp + potD) / 2;
      else
        this->gradientDown = (this->gradientDown + potD) / 2;
    }
    this->last = potD;
    this->count++;
    return potD;
  }
};

Queue q;
Neotimer turntimer = Neotimer(100);
Neotimer meastimer = Neotimer(5);

bool canTurn() {
  return turntimer.done();
}

void turn(Queue& q, ServoDriver* st) {
  st->next();
  st->turn();
  q.act(canTurn, turn, st);
  turntimer.start();
}

bool canMeasure() {
  return meastimer.done();
}

void measure(Queue& q, ServoDriver* st) {    
#ifdef DEBUG
  Serial.print("reading analog in\n");
#endif
  auto val = st->measure();
  //if(val > 10000 || val < -10000) {
    //Serial.print(val); Serial.print('\n');
  //}
  
#ifdef DEBUG
  Serial.print("acting measure\n");
#endif
  q.act(canMeasure, measure, st);
  meastimer.start();
}

void setup() {
  Serial.begin(9600); // 9600 baud is more than enough

  auto st = new ServoDriver(A0, 11);
  q.act(canMeasure, measure, st);
  q.act(canTurn, turn, st);
  turntimer.start();
  meastimer.start();
}

void loop() {
  if(q.alive())
    q.tick();
}
