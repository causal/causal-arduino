struct Branch_ {
public:
  /** checks if branch is still alive.
  * means queued or processed acts are there.
  * if not alive, branch never could recover if not by cross branch processing. */
  virtual bool alive() noexcept = 0;
  /// processor requests to tick next act of branch (thread-safe)
  virtual bool tick() noexcept = 0;
};

struct Queue;
struct _QueueItem {
  _QueueItem* prev = nullptr;
  _QueueItem* next = nullptr;
  void* data;
  bool(*criteria)(void);
  void(*tick)(Queue&, void*);

  _QueueItem(bool(*c)(void), void(*t)(Queue&, void*), void* d)
  : criteria(c), tick(t), data(d) {}
};

struct Queue : public Branch_ {
private:
  _QueueItem* front = nullptr;
  _QueueItem* back = nullptr;
public:
  // act a tick if criteria matches
  bool act(bool(*c)(void), void(*t)(Queue&, void*), void* d = nullptr) {
#ifdef DEBUG
    Serial.print("acting ("); Serial.print(((unsigned int*)t)[0]); Serial.print(";"); Serial.print(((unsigned int*)t)[1]); Serial.print(")");
    if(c) { Serial.print(" if ("); Serial.print(((unsigned int*)c)[0]); Serial.print(","); Serial.print(((unsigned int*)c)[1]); Serial.print(")"); }
    Serial.print('\n');
#endif
    
    _QueueItem* itm = new _QueueItem(c, t, d);

    if(!this->front) {
#ifdef DEBUG
      Serial.print("attaching act to front of branch\n");
#endif
      this->front = itm;
    } else {
#ifdef DEBUG
      Serial.print("attaching act to back of branch\n");
#endif
      itm->prev = this->back;
      this->back->next = itm;
    }

    this->back = itm;

    return true;
  }
  
  bool act(void(*t)(Queue&, void*), void* d = nullptr) {
    this->act(nullptr, t, d);
  }

  /** checks if branch is still alive.
   * means queued or processed acts are there.
   * if not alive, branch never could recover if not by cross branch processing. */
  bool alive() noexcept override{
      return this->front;
  }
  
  /// processor requests to tick next act of branch (thread-safe)
  bool tick() noexcept override {
    _QueueItem* itm = nullptr;

    itm = this->front;
#ifdef DEBUG
    Serial.print("front ("); Serial.print(((unsigned int*)itm->tick)[0]); Serial.print(";"); Serial.print(((unsigned int*)itm->tick)[1]); Serial.print(")");
    if(itm->criteria) { Serial.print(" if ("); Serial.print(((unsigned int*)itm->criteria)[0]); Serial.print(","); Serial.print(((unsigned int*)itm->criteria)[1]); Serial.print(")"); }
    Serial.print('\n');
#endif

    while(itm) {
      if(!itm->criteria || itm->criteria()) {
#ifdef DEBUG
        Serial.print("ticking "); Serial.print(((unsigned int*)itm->tick)[0]); Serial.print(";"); Serial.print(((unsigned int*)itm->tick)[1]);
        Serial.print('\n');
#endif

        // pull item out of queue
        if(this->front == itm)
            this->front = itm->next;

        if(this->back == itm)
            this->back = itm->prev;
        else
            itm->next->prev = itm->prev;

        // do tick
        itm->tick(*this, itm->data);

        delete itm;

        return true;
      } else {  
#ifdef DEBUG
        Serial.print("skipping ("); Serial.print(((unsigned int*)itm->tick)[0]); Serial.print(";"); Serial.print(((unsigned int*)itm->tick)[1]); Serial.print(")");
        Serial.print("because ("); Serial.print(((unsigned int*)itm->criteria)[0]); Serial.print(";"); Serial.print(((unsigned int*)itm->criteria)[1]); Serial.print(") failed");
        Serial.print('\n');
#endif
      }
      
      itm = itm->next;
      
#ifdef DEBUG
      if(itm) {
        Serial.print("next ("); Serial.print(((unsigned int*)itm->tick)[0]); Serial.print(";"); Serial.print(((unsigned int*)itm->tick)[1]); Serial.print(")");
        if(itm->criteria) { Serial.print(" if ("); Serial.print(((unsigned int*)itm->criteria)[0]); Serial.print(","); Serial.print(((unsigned int*)itm->criteria)[1]); Serial.print(")"); }
        Serial.print('\n');
      }
#endif
    }

    return false;
  }  
};
